package com.leroy.ronan.md2wp;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MarkdownToWordpress {

    private String imgRoot;
    private String linkRoot;
    
    public final void setImgRoot(String imgRoot) {
        this.imgRoot = imgRoot;
    }

    public final void setLinkRoot(String linkRoot) {
        this.linkRoot = linkRoot;
    }

    protected String transformLine(String input) {
        List<String> tokenList = new ArrayList<>();

        int curIndex = -1;
        int openBracketIdx = input.indexOf('[', curIndex);
        while (openBracketIdx >= 0) {
            int closeBracketIdx = input.indexOf(']', openBracketIdx);
            int openParenthesisIdx = input.indexOf('(', closeBracketIdx);
            int closeParenthesisIdx = input.indexOf(')', openParenthesisIdx);
            if (closeParenthesisIdx > 0) {
                if (openBracketIdx > 0 && '!' == input.charAt(openBracketIdx-1)){
                    openBracketIdx = openBracketIdx -1;
                }
                tokenList.add(input.substring(curIndex +1, openBracketIdx));
                tokenList.add(input.substring(openBracketIdx, closeParenthesisIdx+1));
                curIndex = closeParenthesisIdx;
                openBracketIdx = input.indexOf('[', curIndex);
            }else {
                break;
            }
        }

        tokenList.add(input.substring(curIndex+1));
        return tokenList.stream().map(s -> transformToken(s)).collect(Collectors.joining());
    }
    
    protected String transformToken(String input) {
        String output = input;
        try {
            if (input.contains("[") && input.contains("]") && input.contains("(") && input.contains(")")) {
                if (input.startsWith("[")) {
                    String text = input.substring(input.indexOf('[')+1, input.indexOf(']'));
                    String link = input.substring(input.indexOf('(')+1, input.indexOf(')'));
                    if (!link.startsWith("http")){
                        output = "["+text+"]("+linkRoot+link+")";
                    }
                } else if (input.startsWith("![")) {
                    String alt = input.substring(input.indexOf('[')+1, input.indexOf(']'));
                    String img = input.substring(input.indexOf('(')+1, input.indexOf(')'));
                    output = "!["+alt+"]("+imgRoot+img+")";
                }
            }
        }catch(Throwable t) {
            System.out.println("unable to transform : "+input);
        }
        return output;

    }

    public static void main(String...args) throws IOException {
        MarkdownToWordpress md2wp = new MarkdownToWordpress();
        md2wp.setImgRoot("https://git.framasoft.org/Ronan/tuto-cucumber/raw/master/");
        md2wp.setLinkRoot("https://git.framasoft.org/Ronan/tuto-cucumber/blob/master/");
        Path input = Paths.get("/users/leroyro1/git/tuto-cucumber/README.md");
        
        String output = Files.readAllLines(input,  Charset.forName("iso-8859-1")).stream().map(s -> md2wp.transformLine(s)).collect(Collectors.joining("\n"));
        Path outputPath = Paths.get("/users/leroyro1/git/tuto-cucumber/README-WP.md");
        Files.write(outputPath, output.getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
    }
}
