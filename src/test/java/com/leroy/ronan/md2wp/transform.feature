Feature: Transform a markdown file into a wordpress file

Background:
Given images root path is "http://www.domain.org/path/to/img/"
Given link root path is "http://www.domain.org/path/to/src/"

Scenario Outline: Transforming a line
Given text is <input>
 When I transform the text
 Then I should get <output>
Examples:
 | input                                                                  | output                                                                 |
 | "nothing special"                                                      | "nothing special"                                                      |
 | "# Title"                                                              | "# Title"                                                              |
 | "## Subtitle"                                                          | "## Subtitle"                                                          |
 | "![Image](image.jpg)"                                                  | "![Image](http://www.domain.org/path/to/img/image.jpg)"                |
 | "[Source](source.java)"                                                | "[Source](http://www.domain.org/path/to/src/source.java)"              |
 | "[Reference](http://www.reference.org/path/to/reference.html)"         | "[Reference](http://www.reference.org/path/to/reference.html)"         |
 | "bla ![Image](image.jpg) bla"                                          | "bla ![Image](http://www.domain.org/path/to/img/image.jpg) bla"        |
 | "bla [Source](source.java) bla"                                        | "bla [Source](http://www.domain.org/path/to/src/source.java) bla"      |
 | "bla [Reference](http://www.reference.org/path/to/reference.html) bla" | "bla [Reference](http://www.reference.org/path/to/reference.html) bla" |
 | "... [...] ..."                                                        | "... [...] ..."                                                        |
 | "[...]"                                                                | "[...]"                                                        |
 
 