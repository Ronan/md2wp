package com.leroy.ronan.md2wp;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MarkdownToWordpressSteps {
    
    private String imgroot;
    private String linkroot;
    
    private String input;
    private String output;
    
    @Given("^images root path is \"(.*)\"$")
    public void images_root_path_is(String root) throws Throwable {
        this.imgroot = root;
    }

    @Given("^link root path is \"([^\"]*)\"$")
    public void link_root_path_is(String linkroot) throws Throwable {
        this.linkroot = linkroot;
    }
    
    @Given("^text is \"(.*)\"$")
    public void text_is(String input) throws Throwable {
        this.input = input;
    }
    
    @When("^I transform the text$")
    public void i_transform_the_text() throws Throwable {
        MarkdownToWordpress md2wp = new MarkdownToWordpress();
        md2wp.setImgRoot(imgroot);
        md2wp.setLinkRoot(linkroot);
        this.output = md2wp.transformLine(this.input);
    }

    @Then("^I should get \"(.*)\"$")
    public void i_should_get(String expected) throws Throwable {
        Assert.assertEquals(expected, this.output);
    }
}
